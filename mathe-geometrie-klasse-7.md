# Mathe Geometrie

## Dreiecke

### Pythagoras

Dies ist **ein** Absatz.

* *Liste* 1
* Liste 2

- Liste 3
- Liste 4

1. Aufzählung
1. Aufzählung
1. Aufzählung
1. Aufzählung

### Quelltext
`Dies ist Quelltext`

### Ein Link
[Pythagoras bei Wikipedia](https://de.wikipedia.org/wiki/Satz_des_Pythagoras)

### Ein Bild aus dem Web
![Darstellung Satz des Pythagoras](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/01-Rechtwinkliges_Dreieck-Pythagoras.svg/390px-01-Rechtwinkliges_Dreieck-Pythagoras.svg.png)

### Ein lokales Bild
![Lizenz-Icons](images/LizenzIcons.png)
Bitte darauf achten:
- Bildname ohne Leerzeichen
- Bildname mit Endung angeben

